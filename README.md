# GSoC / Debian Cloud Image Finder application task

The application task was:

> We'd like to see your HTML and styling + UX skills. Since this is a project starting from scratch you will design and implement a "details page" for the cloud provider of your choice. The outputs of this application task are:
> 
>     * drawing the UX of the "Details page" (mock-up)
>     * implementation of the proposed design using HTML/CSS/bootstrap 

The drawing of the details page is in mockup.svg. I used inkscape for that- i know its not really the ideal tool for wireframe mockups, but on the other hand I have more experience with Inkscape and none with working with mockup programs.

The implementaion of the details page is in the `implementation` folder. I've used the header from [lintian](https://lintian.debian.org). I know that there is a kind of *official* [Debian webdesign](https://wiki.debian.org/DebianWebdesign) but
in my opinion it clashes with Bootstrap. The lintian header on the other hand works oke with Bootstrap. I've not included any javascript, as it is only a prototyp. It uses Bootstraps [list group](https://getbootstrap.com/docs/4.0/components/list-group/)
items and the selected item is folded open.

# Licenses
```
Bootstrap v4.0.0 (https://getbootstrap.com)
Copyright 2011-2018 The Bootstrap Authors
Copyright 2011-2018 Twitter, Inc.
Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
```
Azure Logo from https://upload.wikimedia.org/wikipedia/commons/a/a8/Microsoft_Azure_Logo.svg licensed under http://creativecommons.org/ns#
